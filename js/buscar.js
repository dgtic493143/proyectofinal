$(document).ready(function () {
    var miLista = $("#miLista");
    var busqueda = $("#busqueda");
    $("#btn-buscar").on("click", function () {
        const bus = busqueda.val();
 
        // URL API
        const token = 'eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJhODU2MDEzNmJiZmRmMzUwMmEzNWI2YmU2MzE1YjVhNSIsInN1YiI6IjY1NjJiZWYzZWQ5NmJjMDBhZTQ2ZDExYyIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.Yl7KHkIfy4Ly3GefPIuilhWTiMZMiU-_SeSbW0GCdpo';
        const URL = `https://api.themoviedb.org/3/search/movie?api_key='a8560136bbfdf3502a35b6be6315b5a5'&query=${encodeURIComponent(bus)}`;

        // Header para autorizacion
        const headers = {
            'Authorization': `Bearer ${token}`
        };

        //Solicitud tipo GET a la API
        fetch(URL, { headers })
            .then(response => response.json())
            .then(data => {
                // Vaciamos la lista
                miLista.empty();

                //Se muestran los nombres de las peliculas
                data.results.forEach(movie => {
                    //Elemento para cada pelicula
                    const listItem = $('<li>');

                    //Mostramos la imagen de la película si está disponible
                    if (movie.poster_path) {
                        const img = $(`<img src="https://image.tmdb.org/t/p/w200/${movie.poster_path}" alt="${movie.title}" class="movie-poster">`);
                        listItem.append(img);
                    }

                    //Se crea un contenedor para la información
                    const contenedor = $('<div class="movie-info">');

                    //Se muestra el nombre de la película
                    const titulo = $(`<p>${movie.title}</p>`);
                    contenedor.append(titulo);

                    //Se muestra descripción de la película
                    if (movie.overview) {
                        const descripcion = $(`<p>${movie.overview}</p>`);
                        contenedor.append(descripcion);
                    }

                    //Incluir el contenedor en el objeto de la lista
                    listItem.append(contenedor);
                    //Inlcuir el objeto de la lista a la lista principal de la busqueda
                    miLista.append(listItem);
                });
            })
            .catch(error => {
                console.error('Error al realizar la búsqueda:', error);
            });
    });
});