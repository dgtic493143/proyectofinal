Tsutsumi Bernal David Israel

En este codigo se utiliza jQuery para poder realizar una búsqueda de las películas a traves de la API de TMDB.
Al cargar la página, se preparan referencias a elementos del DOM como la lista (miLista) y el campo de búsqueda (busqueda).
Cuando el usuario hace clic en el botón de búsqueda, se extrae la palabra clave que ingreso. Luego, se construye la URL de la API de TMBD con la palabra clave y se realiza una solicitud GET utilizando fetch, incluyendo un token de autorización en los encabezados. La respuesta de la API se maneja convirtiéndola a JSON, y la lista de películas en el DOM se vacía para mostrar los nuevos resultados. 
Para cada película en los resultados, se crea un elemento de lista que incluye la imagen si esta se encuentra disponible, el título y la descripción de las peliculas. Estos elementos se agregan a la lista principal de películas en la página web. Si hay algún error durante la solicitud a la API, se muestra un mensaje de error en la consola.
